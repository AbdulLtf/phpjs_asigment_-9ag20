const ResponseHelper = require("../Helpers/responseHelper");
const dtl_Product = require("../DataLayer/dtl_Product");
const Product = {
  readProductHandler: (req, res, next) => {
    dtl_Product.readProductHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  createProductHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Product.createProductHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  deleteProductHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Product.deleteProductHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  editProductHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Product.editProductHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  searchProductHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Product.searchProductHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  }
};
module.exports = Product;
