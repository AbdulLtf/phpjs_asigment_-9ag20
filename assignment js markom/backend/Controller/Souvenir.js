const ResponseHelper = require("../Helpers/responseHelper");
const dtl_Souvenir = require("../DataLayer/dtl_Souvenir");
const Product = {
  readSouvenirHandler: (req, res, next) => {
    dtl_Souvenir.readSouvenirHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  readUnitHandler: (req, res, next) => {
    dtl_Souvenir.readUnitHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  createSouvenirHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Souvenir.createSouvenirHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  deleteSouvenirHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Souvenir.deleteSouvenirHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  editSouvenirHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Souvenir.editSouvenirHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  searchSouvenirHandler: (req, res, next) => {
    var docs = req.body;
    dtl_Souvenir.searchSouvenirHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  }
};
module.exports = Product;
