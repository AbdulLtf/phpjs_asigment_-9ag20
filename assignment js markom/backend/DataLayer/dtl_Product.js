const pg = require("pg");
const DatabaseConnection = require("../Config/dbp.config.json");
var DB = new pg.Pool(DatabaseConnection.config);
const dtl_Product = {
  readProductHandlerData: callback => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      client.query(
        `select * from product where is_delete = false order by id asc `,
        function (err, result) {
          done();
          if (err) {
            data = err;
          } else {
            for (i = 0; i < result.rows.length; i++) {
              let createdate = result.rows[i].created_date.toLocaleDateString();
              createdate = createdate.split("/");
              createdate =
                createdate[0] + "/" + createdate[1] + "/" + createdate[2];
              creatdates = createdate.split("undefined");
              result.rows[i].created_date = creatdates;
            }
            data = result.rows;
          }
          callback(data);
        }
      );
    });
  },
  createProductHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `INSERT INTO product (name,description,is_delete,created_by,created_date)
               VALUES ($1, $2, false, 'latief', current_date)`,
        values: [docs.name, docs.description]
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diinput";
        }
        callback(data);
      });
    });
  },
  deleteProductHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `UPDATE product SET is_delete = true WHERE id = $1`,
        values: [docs.id]
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil di set menjadi true";
        }
        callback(data);
      });
    });
  },
  editProductHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      console.log(docs);
      const query = {
        text: `UPDATE product
        SET name=$1,description = $2,updated_by='latief',updated_date = current_date
        WHERE id = $3`,
        values: [docs.name, docs.description, docs.id]
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diubah ";
        }
        callback(data);
      });
    });
  },
  searchProductHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      search1 = "%" + docs.search1 + "%";
      search2 = "%" + docs.search2 + "%";
      search3 = docs.search3;
      search4 = "%" + docs.search4 + "%";
      search5 = "%" + docs.search5 + "%";
      if (search1 === "%%" || docs.search1 === "") {
        search1 = null;
      }
      if (docs.search2 === "%%" || docs.search2 === "") {
        search2 = null;
      }
      if (docs.search3 === "") {
        search3 = null;
      }
      if (docs.search4 === "%%" || docs.search4 === "") {
        search4 = null;
      }
      if (docs.search5 === "%%" || docs.search5 === "") {
        search5 = null;
      }

      const query = {
        text: `SELECT * from product
        WHERE is_delete=false and (LOWER(code) like LOWER($1) 
        or LOWER(name) like LOWER($2) 
        or created_date=($3)
        or LOWER(description) like LOWER($4) 
        or LOWER(created_by) like LOWER($5) )
        ORDER BY id asc`,

        values: [search1, search2, search3, search4, search5]
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          for (i = 0; i < result.rows.length; i++) {
            let createdate = result.rows[i].created_date.toLocaleDateString();
            createdate = createdate.split("/");
            createdate =
              createdate[0] + "/" + createdate[1] + "/" + createdate[2];
            creatdates = createdate.split("undefined");
            result.rows[i].created_date = creatdates;
          }
          data = result.rows;
        }
        callback(data);
      });
    });
  }
};
module.exports = dtl_Product;
