const ProductLogic = require("../Controller/Product");
const SouvenirLogic = require("../Controller/Souvenir");

module.exports = exports = function (server) {
  server.get("/api/getproduct", ProductLogic.readProductHandler);
  server.post("/api/postproduct", ProductLogic.createProductHandler);
  server.put("/api/deleteproduct", ProductLogic.deleteProductHandler);
  server.put("/api/editproduct", ProductLogic.editProductHandler);
  server.post("/api/searchproduct", ProductLogic.searchProductHandler);

  server.get("/api/getsouvenir", SouvenirLogic.readSouvenirHandler);
  server.get("/api/getunit", SouvenirLogic.readUnitHandler);
  server.post("/api/postsouvenir", SouvenirLogic.createSouvenirHandler);
  server.put("/api/deletesouvenir", SouvenirLogic.deleteSouvenirHandler);
  server.put("/api/editsouvenir", SouvenirLogic.editSouvenirHandler);
  server.post("/api/searchsouvenir", SouvenirLogic.searchSouvenirHandler);
};
