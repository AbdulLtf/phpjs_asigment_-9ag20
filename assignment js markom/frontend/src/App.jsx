import React from "react";
import { Switch, Route } from "react-router-dom";
import Dashboard from "./component/Dashboard";
import Sidebar from "./component/Sidebar";
import Product from "./component/content/product/listProduct";
import Souvenir from "./component/content/souvenir/listSouvenir";

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/" component={Dashboard} />
        <Route path="/product" component={Dashboard} />
        <Route path="/product" component={Sidebar} />
        <Route path="/product" component={Product} />

        <Route path="/souvenir" component={Dashboard} />
        <Route path="/souvenir" component={Sidebar} />
        <Route path="/souvenir" component={Souvenir} />
      </Switch>
    );
  }
}
export default App;
