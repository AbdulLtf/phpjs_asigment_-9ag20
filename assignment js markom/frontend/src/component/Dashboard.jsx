import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Sidebar from "./Sidebar";
import Home from "./content/home";
import Souvenir from "./content/souvenir/listSouvenir";
import Product from "./content/product/listProduct";

class Dashboard extends React.Component {
  render() {
    return (
      <div style={{ backgroundColor: "grey", height: "675px" }}>
        <div className="mt-1" style={{ margin: "2em" }}>
          <div className="row">
            <Router>
              <div className="col-lg-2">
                <Sidebar />
              </div>
              <div className="col-lg-10">
                <div
                  style={{
                    height: "650px",
                    width: "100%",
                    backgroundColor: "white",
                    overflow: "auto"
                  }}
                >
                  <section className="content">
                    <Switch>
                      <Route path="/home">
                        <Home />
                      </Route>{" "}
                      <Route path="/product">
                        <Product />
                      </Route>{" "}
                      <Route path="/souvenir">
                        <Souvenir />
                      </Route>{" "}
                    </Switch>
                  </section>
                </div>
              </div>
            </Router>
          </div>
        </div>
      </div>
    );
  }
}
export default Dashboard;
