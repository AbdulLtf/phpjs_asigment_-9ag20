import React from "react";
import apiconfig from "../../../configs/api.configs.json";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Container,
  Row,
  Col
} from "reactstrap";
import { withRouter } from "react-router-dom";
import axios from "axios";

class Hapus extends React.Component {
  constructor(props) {
    super(props);
    this.submitHandler = this.submitHandler.bind(this);
  }

  submitHandler() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.DELETEPRODUCT,
      method: "put",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      },
      data: { id: this.props.product.id }
    };
    axios(option)
      .then(response => {
        if (response.data.code === 200) {
        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        console.log(error);
      });

    this.props.closeModalHandler();
  }

  render() {
    return (
      <Modal
        style={{ color: "#ffffff", backgroundColor: "#ff0000" }}
        isOpen={this.props.delete}
        className={this.props.className}
      >
        <ModalHeader style={{ color: "#ffffff", backgroundColor: "#ff0000" }}>
          Hapus ?
        </ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col xs="auto">
                <span
                  xs="auto"
                  class="fa fa-trash"
                  style={{ fontSize: "50px", color: "#ff0000" }}
                />
              </Col>
              <Col xs="auto">
                <Row>
                  <Col xs="auto" style={{ color: "#ff0000" }}>
                    {" "}
                    Apa anda yakin Ingin menghapus data Product?
                  </Col>
                </Row>
                <Row>
                  <Col
                    xs="auto"
                    style={{
                      marginLeft: "10%",
                      fontWeight: "Bold",
                      color: "#ff0000"
                    }}
                  >
                    {this.props.product.name} dengan Code Product{" "}
                    {this.props.product.code}
                  </Col>
                </Row>{" "}
              </Col>
            </Row>
          </Container>
        </ModalBody>
        <ModalFooter>
          <Button
            style={{ color: "#ffffff", backgroundColor: "#ff0000" }}
            onClick={this.submitHandler}
          >
            Ya
          </Button>
          <Button color="warning" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
export default withRouter(Hapus);
