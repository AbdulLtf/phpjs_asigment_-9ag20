import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { withRouter } from "react-router-dom";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";

class EditProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formdata: {
        id: "",
        name: "",
        description: ""
      }
    };
    this.submitHandler = this.submitHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp
    });
  }

  submitHandler() {
    const id = this.props.product.id;
    let name = this.state.formdata.name;
    let description = this.state.formdata.description;

    if (name === "") {
      name = this.props.product.name;
    }
    if (description === "") {
      description = this.props.product.description;
    }
    // alert(JSON.stringify(this.state.formdata));
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.EDITPRODUCT,
      method: "put",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      },
      data: {
        name: name,
        description: description,
        id: id
      }
    };
    axios(option)
      .then(response => {
        alert("success");
        if (response.data.code === 200) {
          alert("data sudah dimasukan");
        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.props.closeModalHandler();
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.edit}
          className={this.props.className}
          size="md"
        >
          <ModalHeader
            className="custom-modal-style"
            style={{ color: "#ffffff", backgroundColor: "#000066" }}
          >
            Edit Product ({this.props.product.code})
          </ModalHeader>
          <ModalBody>
            <div className="container">
              <div className="row" style={{ width: "710px" }}>
                <div className="mb-2 col-lg-12" style={{ display: "block" }}>
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Product Code</div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        disabled
                        placeholder={this.props.product.code}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "5px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Product Name : </div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        class="form-control"
                        name="name"
                        required
                        value={this.state.formdata.name}
                        placeholder={this.props.product.name}
                        onChange={this.changeHandler}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "5px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">
                      Product Description
                    </div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        // disabled
                        // placeholder={this.props.product.description}
                        type="text"
                        class="form-control"
                        name="description"
                        required
                        value={this.state.formdata.description}
                        placeholder={this.props.product.description}
                        onChange={this.changeHandler}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "3px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                </div>
              </div>
            </div>
          </ModalBody>
          <ModalFooter style={{ backgroundColor: "#e9ecef" }}>
            <Button color="primary" onClick={this.submitHandler}>
              Update
            </Button>
            <Button color="Warning" onClick={this.props.closeModalHandler}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default withRouter(EditProduct);
