import React from "react";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";
import { Row, Col, Input } from "reactstrap";
import { withRouter, Link } from "react-router-dom";
import CreateProduct from "./createProduct";
import ViewProduct from "./viewProduct";
import EditProduct from "./editProduct";
import DeleteProduct from "./deleteProduct";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
      productlist: [],
      currentProduct: {},
      formdata: {
        search1: "",
        search2: "",
        search3: "",
        search4: "",
        search5: ""
      },
      viewProduct: false,
      editProduct: false,
      createProduct: false,
      deleteProduct: false
    };
    this.getListProduct = this.getListProduct.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.getListProductByCode = this.getListProductByCode.bind(this);
    this.showHandler = this.showHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
    this.viewModalHandler = this.viewModalHandler.bind(this);
    this.editModalHandler = this.editModalHandler.bind(this);
  }
  componentDidMount() {
    this.getListProduct();
  }

  closeModalHandler() {
    this.setState({
      viewProduct: false,
      editProduct: false,
      createProduct: false,
      deleteProduct: false
    });

    this.props.history.push("/product");
    this.getListProduct();
  }

  showHandler() {
    this.setState({ createProduct: true });
  }

  deleteModalHandler(idUD) {
    let tmp = {};
    this.state.product.map(row => {
      if (idUD === row.id) {
        tmp = row;
      }
      return true;
    });
    this.setState({
      currentProduct: tmp,
      deleteProduct: true
    });
  }

  viewModalHandler(idUD) {
    let tmp = {};
    this.state.product.map(row => {
      if (idUD === row.id) {
        tmp = row;
      }
      return true;
    });
    this.setState({
      currentProduct: tmp,
      viewProduct: true
    });
  }
  editModalHandler(idUD) {
    this.state.product.map(row => {
      if (idUD === row.id) {
        this.setState({
          currentProduct: row,
          editProduct: true
        });
      }
      return true;
    });
  }

  updateSearch(event) {
    // supaya setiap dipanggil menghapus seluruh search
    // terlebih dahulu
    this.setState({
      formdata: {
        search1: "",
        search2: "",
        search3: "",
        search4: "",
        search5: ""
      }
    });
    let tmp = this.state.formdata;
    tmp[event.target.name] = event.target.value;
    this.setState({
      product: [],
      productlist: [],
      formdata: tmp
    });
  }

  getListProduct() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETPRODUCT,
      method: "get",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      }
    };
    axios(option)
      .then(response => {
        if (response == null) {
          alert("tidak ada data");
        } else {
          let tmp = [];
          let tmp2 = [];
          response.data.message.map(row => {
            let c = (
              <Link to="#">
                <span
                  onClick={() => {
                    this.viewModalHandler(row.id);
                  }}
                  className="fa fa-search"
                  style={{ fontSize: "18px", paddingRight: "30px" }}
                ></span>
                <span
                  onClick={() => {
                    this.editModalHandler(row.id);
                  }}
                  className="fa fa-edit"
                  style={{ fontSize: "18px", paddingRight: "30px" }}
                ></span>
                <span
                  onClick={() => {
                    this.deleteModalHandler(row.id);
                  }}
                  className="fa fa-trash"
                  style={{ fontSize: "18px", paddingRight: "30px" }}
                ></span>
              </Link>
            );
            tmp.push({
              id: row.id,
              name: row.name,
              code: row.code,
              description: row.description,
              created_date: row.created_date,
              created_by: row.created_by
            });
            tmp2.push({
              id: row.id,
              name: row.name,
              code: row.code,
              description: row.description,
              created_date: row.created_date,
              created_by: row.created_by,
              action: c
            });
            return true;
          });

          this.setState({
            product: tmp,
            productlist: tmp2
          });
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  getListProductByCode() {
    // alert(JSON.stringify(this.state.formdata));
    if (
      this.state.formdata.search1 === "" &&
      this.state.formdata.search2 === "" &&
      this.state.formdata.search3 === "" &&
      this.state.formdata.search4 === "" &&
      this.state.formdata.search5 === ""
    ) {
      this.setState({
        product: [],
        productlist: []
      });
      this.getListProduct();
    } else {
      let token = localStorage.getItem(apiconfig.LS.TOKEN);
      let option = {
        url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.SEARCHPRODUCT,
        method: "post",
        headers: {
          Authorization: token,
          "Content-Type": "application/json"
        },
        data: {
          search1: this.state.formdata.search1,
          search2: this.state.formdata.search2,
          search3: this.state.formdata.search3,
          search4: this.state.formdata.search4,
          search5: this.state.formdata.search5
        }
      };
      axios(option)
        .then(response => {
          if (response == null) {
            alert("tidak ada data");
          } else {
            let tmp = [];
            let tmp2 = [];
            response.data.message.map(row => {
              let c = (
                <Link to="#">
                  <span
                    onClick={() => {
                      this.viewModalHandler(row.id);
                    }}
                    className="fa fa-search"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.editModalHandler(row.id);
                    }}
                    className="fa fa-edit"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.deleteModalHandler(row.id);
                    }}
                    className="fa fa-trash"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                </Link>
              );
              tmp.push({
                id: row.id,
                name: row.name,
                code: row.code,
                description: row.description,
                created_date: row.created_date,
                created_by: row.created_by
              });
              tmp2.push({
                id: row.id,
                name: row.name,
                code: row.code,
                description: row.description,
                created_date: row.created_date,
                created_by: row.created_by,
                action: c
              });
              return true;
            });

            this.setState({
              product: tmp,
              productlist: tmp2
            });
          }
        })
        .catch(error => {
          alert(error);
        });
    }
  }
  render() {
    return (
      <div style={{ height: "212px", width: "100%" }}>
        <div
          class="container"
          style={{ marginTop: "10px", width: "100%", height: "50%" }}
        >
          <center>
            <div>
              <ol class="breadcrumb float-sm-right">
                <br></br>
                <li class="breadcrumb-item">
                  <a href="/home">Home</a>
                </li>
                <li class="breadcrumb-item active">List Product</li>
              </ol>
              <h4>LIST PRODUCT</h4>
            </div>
          </center>
          <CreateProduct
            create={this.state.createProduct}
            closeModalHandler={this.closeModalHandler}
          />
          <ViewProduct
            view={this.state.viewProduct}
            closeModalHandler={this.closeModalHandler}
            product={this.state.currentProduct}
          />
          <EditProduct
            edit={this.state.editProduct}
            closeModalHandler={this.closeModalHandler}
            product={this.state.currentProduct}
          />
          <DeleteProduct
            delete={this.state.deleteProduct}
            closeModalHandler={this.closeModalHandler}
            product={this.state.currentProduct}
          />

          <div className="jumbotron">
            <div className="container">
              <Row
                style={{
                  borderBottomStyle: "ridge",
                  borderBottomColor: "#000066"
                }}
              >
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "140px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Code
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search1"
                      placeholder="Berdasarkan Code"
                      value={this.state.formdata.search1}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "140px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Nama
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search2"
                      placeholder="Berdasarkan Nama"
                      value={this.state.formdata.search2}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "120px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian tanggal
                  </Row>
                  <Row>
                    <Input
                      type="date"
                      class="form-control"
                      reqiured
                      name="search3"
                      value={this.state.formdata.search3}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "150px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Description
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search4"
                      placeholder="Berdasarkan Deskripsi"
                      value={this.state.formdata.search4}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "170px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Pembuat
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search5"
                      placeholder="Berdasarkan Pembuat"
                      value={this.state.formdata.search5}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "5px",
                    columnWidth: "10px",
                    marginTop: "-17px"
                  }}
                  className="d-inline-block"
                >
                  <span
                    className="btn btn-warning "
                    onClick={this.getListProductByCode}
                    style={{
                      fontSize: "20px",
                      height: "35px",
                      color: "white",
                      textAlign: "center"
                    }}
                  >
                    Cari
                  </span>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "45px",
                    columnWidth: "10px",
                    marginTop: "-17px"
                  }}
                  className="d-inline-block"
                >
                  <span
                    className="btn btn-primary d-inline-block"
                    onClick={this.showHandler}
                    style={{
                      fontSize: "20px",
                      height: "35px",
                      color: "white",
                      textAlign: "center"
                    }}
                  >
                    Add
                  </span>
                </Col>
              </Row>
              <Row>
                <table
                  style={{ borderStyle: "insert" }}
                  id="mytable"
                  class="table table-bordered table-striped"
                >
                  <thead>
                    <tr>
                      <th>
                        <center>No</center>
                      </th>
                      <th>
                        <center>Kode Product</center>
                      </th>
                      <th>
                        <center>Nama Product</center>
                      </th>
                      <th>
                        <center>Description</center>
                      </th>
                      <th>
                        <center>Created Date</center>
                      </th>
                      <th>
                        <center>Created By</center>
                      </th>
                      <th>
                        <center>Action</center>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.productlist === "" ? (
                      <tr>
                        <td
                          style={{ color: "red", fontWeight: "bold" }}
                          colSpan="5"
                          rowSpan="5"
                        >
                          <h1>DATA TIDAK DITEMUKAN</h1>
                        </td>
                      </tr>
                    ) : (
                      this.state.productlist.map((row, x) => (
                        <tr>
                          <td>
                            <center>{x + 1}</center>
                          </td>
                          <td>
                            <center>{row.code}</center>
                          </td>
                          <td>
                            <center>{row.name}</center>
                          </td>
                          <td>
                            <center>{row.description}</center>
                          </td>
                          <td>
                            <center>{row.created_date}</center>
                          </td>
                          <td>
                            <center>{row.created_by}</center>
                          </td>
                          <td>
                            <center>{row.action}</center>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(Product);
