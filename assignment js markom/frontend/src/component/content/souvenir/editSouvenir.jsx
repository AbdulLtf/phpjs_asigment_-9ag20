import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { withRouter } from "react-router-dom";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";

class EditSouvenir extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      unit: [],
      formdata: {
        code: "",
        name: "",
        description: "",
        unit_id: ""
      }
    };
    this.submitHandler = this.submitHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.getListUnit = this.getListUnit.bind(this);
  }
  componentDidMount() {
    this.getListUnit();
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp
    });
  }

  getListUnit() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETUNIT,
      method: "get",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      }
    };
    axios(option)
      .then(response => {
        if (response == null) {
          alert("tidak ada data");
        } else {
          let tmp = [];
          response.data.message.map(row => {
            tmp.push({
              id: row.id,
              name: row.name
            });
            return true;
          });
          this.setState({
            unit: tmp
          });
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  submitHandler() {
    alert(JSON.stringify(this.props.souvenir));
    const id = this.props.souvenir.id;
    let name = this.state.formdata.name;
    let description = this.state.formdata.description;
    let unit_id = this.state.formdata.unit_id;
    if (unit_id === "") {
      unit_id = this.props.souvenir.unit_id;
    }
    if (name === "") {
      name = this.props.souvenir.name;
    }
    if (description === "") {
      description = this.props.souvenir.description;
    }

    // alert(id);
    alert(JSON.stringify(this.state.formdata));
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.EDITSOUVENIR,
      method: "put",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      },
      data: {
        name: name,
        description: description,
        unit_id: unit_id,
        id: id
      }
    };
    axios(option)
      .then(response => {
        alert("success");
        if (response.data.code === 200) {
          alert("data sudah dimasukan");
        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        console.log(error);
      });

    this.props.closeModalHandler();
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.edit}
          className={this.props.className}
          size="md"
        >
          <ModalHeader
            className="custom-modal-style"
            style={{ color: "#ffffff", backgroundColor: "#000066" }}
          >
            Edit Souvenir ({this.props.souvenir.code})
          </ModalHeader>
          <ModalBody>
            <div className="container">
              <div className="row" style={{ width: "710px" }}>
                <div className="mb-2 col-lg-12" style={{ display: "block" }}>
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Souvenir Code</div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        disabled
                        placeholder={this.props.souvenir.code}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "5px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Souvenir Name : </div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        class="form-control"
                        name="name"
                        required
                        value={this.state.formdata.name}
                        placeholder={this.props.souvenir.name}
                        onChange={this.changeHandler}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "5px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">
                      Souvenir Description
                    </div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        class="form-control"
                        name="description"
                        required
                        value={this.state.formdata.description}
                        placeholder={this.props.souvenir.description}
                        onChange={this.changeHandler}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "3px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <div className="col-3 d-inline-block">Souvenir Unit</div>
                  <div className="col-1 d-inline-block">:</div>
                  <div className="col-3 d-inline-block">
                    <select
                      className="form-control"
                      name="unit_id"
                      onChange={this.changeHandler}
                      value={this.state.formdata.unit_id}
                      style={{
                        width: "150px"
                      }}
                    >
                      <option disabled value="">
                        {this.props.souvenir.un_name}
                      </option>
                      {this.state.unit.map((row, x) => (
                        <option value={row.id}>{row.name}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </ModalBody>
          <ModalFooter style={{ backgroundColor: "#e9ecef" }}>
            <Button color="primary" onClick={this.submitHandler}>
              Update
            </Button>
            <Button color="Warning" onClick={this.props.closeModalHandler}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default withRouter(EditSouvenir);
