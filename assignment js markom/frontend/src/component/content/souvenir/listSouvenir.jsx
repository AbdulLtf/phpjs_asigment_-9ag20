import React from "react";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";
import { Row, Col, Input } from "reactstrap";
import { withRouter, Link } from "react-router-dom";
import CreateSouvenir from "./createSouvenir";
import ViewSouvenir from "./viewSouvenir";
import EditSouvenir from "./editSouvenir";
import DeleteSouvenir from "./deleteSouvenir";

class ListSouvenir extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      souvenir: [],
      souvenirlist: [],
      currentSouvenir: {},
      unit: [],
      formdata: {
        search1: "",
        search2: "",
        search3: "",
        search4: "",
        search5: ""
      },
      viewSouvenir: false,
      editSouvenir: false,
      createSouvenir: false,
      deleteSouvenir: false
    };
    this.getListSouvenir = this.getListSouvenir.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.getListSouvenirBySearch = this.getListSouvenirBySearch.bind(this);
    this.getListUnit = this.getListUnit.bind(this);
    this.showHandler = this.showHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
    this.viewModalHandler = this.viewModalHandler.bind(this);
    this.editModalHandler = this.editModalHandler.bind(this);
  }
  componentDidMount() {
    this.getListSouvenir();
    this.getListUnit();
  }

  closeModalHandler() {
    this.getListSouvenir();
    this.getListSouvenir();
    this.setState({
      viewSouvenir: false,
      deleteSouvenir: false,
      createSouvenir: false,
      editSouvenir: false
    });

    this.props.history.push("/souvenir");
  }

  showHandler() {
    this.setState({ createSouvenir: true });
  }

  deleteModalHandler(idUD) {
    let tmp = {};
    this.state.souvenir.map(row => {
      if (idUD === row.id) {
        tmp = row;
      }
      return true;
    });
    this.setState({
      currentSouvenir: tmp,
      deleteSouvenir: true
    });
  }

  viewModalHandler(idUD) {
    let tmp = {};
    this.state.souvenir.map(row => {
      if (idUD === row.id) {
        tmp = row;
      }
      return true;
    });
    this.setState({
      currentSouvenir: tmp,
      viewSouvenir: true
    });
  }
  editModalHandler(idUD) {
    this.state.souvenir.map(row => {
      if (idUD === row.id) {
        this.setState({
          currentSouvenir: row,
          editSouvenir: true
        });
      }
      return true;
    });
  }

  updateSearch(event) {
    // supaya setiap dipanggil menghapus seluruh search
    // terlebih dahulu
    this.setState({
      formdata: {
        search1: "",
        search2: "",
        search3: "",
        search4: "",
        search5: ""
      }
    });
    let tmp = this.state.formdata;
    tmp[event.target.name] = event.target.value;
    this.setState({
      souvenir: [],
      souvenirlist: [],
      formdata: tmp
    });
  }

  getListUnit() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETUNIT,
      method: "get",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      }
    };
    axios(option)
      .then(response => {
        if (response == null) {
          alert("tidak ada data");
        } else {
          let tmp = [];
          response.data.message.map(row => {
            tmp.push({
              id: row.id,
              name: row.name
            });
            return true;
          });
          this.setState({
            unit: tmp
          });
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  getListSouvenir() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETSOUVENIR,
      method: "get",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      }
    };
    axios(option)
      .then(response => {
        if (response == null) {
          alert("tidak ada data");
        } else {
          let tmp = [];
          let tmp2 = [];
          response.data.message.map(row => {
            let c = (
              <Link to="#">
                <span
                  onClick={() => {
                    this.viewModalHandler(row.id);
                  }}
                  className="fa fa-search"
                  style={{ fontSize: "18px", paddingRight: "30px" }}
                ></span>
                <span
                  onClick={() => {
                    this.editModalHandler(row.id);
                  }}
                  className="fa fa-edit"
                  style={{ fontSize: "18px", paddingRight: "30px" }}
                ></span>
                <span
                  onClick={() => {
                    this.deleteModalHandler(row.id);
                  }}
                  className="fa fa-trash"
                  style={{ fontSize: "18px", paddingRight: "30px" }}
                ></span>
              </Link>
            );
            tmp.push({
              id: row.id,
              name: row.name,
              code: row.code,
              description: row.description,
              created_date: row.created_date,
              created_by: row.created_by,
              unit_id: row.unit_id,
              un_name: row.un_name,
              un_id: row.un_id
            });
            tmp2.push({
              id: row.id,
              name: row.name,
              code: row.code,
              description: row.description,
              created_date: row.created_date,
              created_by: row.created_by,
              unit_id: row.unit_id,
              un_name: row.un_name,
              un_id: row.un_id,
              action: c
            });
            return true;
          });

          this.setState({
            souvenir: tmp,
            souvenirlist: tmp2
          });
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  getListSouvenirBySearch() {
    // alert(JSON.stringify(this.state.formdata));
    if (
      this.state.formdata.search1 === "" &&
      this.state.formdata.search2 === "" &&
      this.state.formdata.search3 === "" &&
      this.state.formdata.search4 === "" &&
      this.state.formdata.search5 === ""
    ) {
      this.setState({
        souvenir: [],
        souvenirlist: []
      });
      this.getListSouvenir();
    } else {
      let token = localStorage.getItem(apiconfig.LS.TOKEN);
      let option = {
        url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.SEARCHSOUVENIR,
        method: "post",
        headers: {
          Authorization: token,
          "Content-Type": "application/json"
        },
        data: {
          search1: this.state.formdata.search1,
          search2: this.state.formdata.search2,
          search3: this.state.formdata.search3,
          search4: this.state.formdata.search4,
          search5: this.state.formdata.search5
        }
      };
      axios(option)
        .then(response => {
          if (response == null) {
            alert("tidak ada data");
          } else {
            let tmp = [];
            let tmp2 = [];
            response.data.message.map(row => {
              let c = (
                <Link to="#">
                  <span
                    onClick={() => {
                      this.viewModalHandler(row.id);
                    }}
                    className="fa fa-search"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.editModalHandler(row.id);
                    }}
                    className="fa fa-edit"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.deleteModalHandler(row.id);
                    }}
                    className="fa fa-trash"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                </Link>
              );
              tmp.push({
                id: row.id,
                name: row.name,
                code: row.code,
                description: row.description,
                created_date: row.created_date,
                created_by: row.created_by,
                unit_id: row.unit_id,
                un_name: row.un_name,
                un_id: row.un_id
              });
              tmp2.push({
                id: row.id,
                name: row.name,
                code: row.code,
                description: row.description,
                created_date: row.created_date,
                created_by: row.created_by,
                unit_id: row.unit_id,
                un_name: row.un_name,
                un_id: row.un_id,
                action: c
              });
              return true;
            });

            this.setState({
              souvenir: tmp,
              souvenirlist: tmp2
            });
          }
        })
        .catch(error => {
          alert(error);
        });
    }
  }
  render() {
    return (
      <div style={{ height: "212px", width: "100%" }}>
        <div
          class="container"
          style={{ marginTop: "10px", width: "100%", height: "50%" }}
        >
          <center>
            <div>
              <ol class="breadcrumb float-sm-right">
                <br></br>
                <li class="breadcrumb-item">
                  <a href="/home">Home</a>
                </li>
                <li class="breadcrumb-item active">List Souvenir</li>
              </ol>
              <h4>LIST SOUVENIR</h4>
            </div>
          </center>
          <ViewSouvenir
            view={this.state.viewSouvenir}
            closeModalHandler={this.closeModalHandler}
            souvenir={this.state.currentSouvenir}
          />
          <DeleteSouvenir
            delete={this.state.deleteSouvenir}
            closeModalHandler={this.closeModalHandler}
            souvenir={this.state.currentSouvenir}
          />
          <CreateSouvenir
            create={this.state.createSouvenir}
            closeModalHandler={this.closeModalHandler}
          />
          <EditSouvenir
            edit={this.state.editSouvenir}
            closeModalHandler={this.closeModalHandler}
            souvenir={this.state.currentSouvenir}
          />

          <div className="jumbotron">
            <div className="container">
              <Row
                style={{
                  borderBottomStyle: "ridge",
                  borderBottomColor: "#000066"
                }}
              >
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "140px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Code
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search1"
                      placeholder="Berdasarkan Code"
                      value={this.state.formdata.search1}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "140px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Nama
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search2"
                      placeholder="Berdasarkan Nama"
                      value={this.state.formdata.search2}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "150px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Unit
                  </Row>
                  <Row>
                    <select
                      className="form-control"
                      name="search3"
                      onChange={this.updateSearch}
                      value={this.state.formdata.search3}
                      style={{
                        width: "150px"
                      }}
                    >
                      <option value="">-Select Unit Name-</option>
                      {this.state.unit.map((row, x) => (
                        <option value={row.id}>{row.name}</option>
                      ))}
                    </select>
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "120px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian tanggal
                  </Row>
                  <Row>
                    <Input
                      type="date"
                      class="form-control"
                      reqiured
                      name="search4"
                      value={this.state.formdata.search4}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "10px",
                    columnWidth: "170px"
                  }}
                >
                  <Row style={{ marginTop: "-40px", fontSize: "15px" }}>
                    Pencarian Pembuat
                  </Row>
                  <Row>
                    <Input
                      type="text"
                      class="form-control"
                      reqiured
                      name="search5"
                      placeholder="Berdasarkan Pembuat"
                      value={this.state.formdata.search5}
                      onChange={this.updateSearch}
                    />
                  </Row>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "5px",
                    columnWidth: "10px",
                    marginTop: "-17px"
                  }}
                  className="d-inline-block"
                >
                  <span
                    className="btn btn-warning "
                    onClick={this.getListSouvenirBySearch}
                    style={{
                      fontSize: "20px",
                      height: "35px",
                      color: "white",
                      textAlign: "center"
                    }}
                  >
                    Cari
                  </span>
                </Col>
                <Col
                  xs="auto"
                  style={{
                    marginLeft: "45px",
                    columnWidth: "10px",
                    marginTop: "-17px"
                  }}
                  className="d-inline-block"
                >
                  <span
                    className="btn btn-primary d-inline-block"
                    onClick={this.showHandler}
                    style={{
                      fontSize: "20px",
                      height: "35px",
                      color: "white",
                      textAlign: "center"
                    }}
                  >
                    Add
                  </span>
                </Col>
              </Row>
              <Row>
                <table
                  style={{ borderStyle: "insert" }}
                  id="mytable"
                  class="table table-bordered table-striped"
                >
                  <thead>
                    <tr>
                      <th>
                        <center>No</center>
                      </th>
                      <th>
                        <center>Kode Souvenir</center>
                      </th>
                      <th>
                        <center>Nama Souvenir</center>
                      </th>
                      <th>
                        <center>Unit</center>
                      </th>
                      <th>
                        <center>Created Date</center>
                      </th>
                      <th>
                        <center>Created By</center>
                      </th>
                      <th>
                        <center>Action</center>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.souvenirlist === "" ? (
                      <tr>
                        <td
                          style={{ color: "red", fontWeight: "bold" }}
                          colSpan="5"
                          rowSpan="5"
                        >
                          <h1>DATA TIDAK DITEMUKAN</h1>
                        </td>
                      </tr>
                    ) : (
                      this.state.souvenirlist.map((row, x) => (
                        <tr>
                          <td>
                            <center>{x + 1}</center>
                          </td>
                          <td>
                            <center>{row.code}</center>
                          </td>
                          <td>
                            <center>{row.name}</center>
                          </td>
                          <td>
                            <center>{row.un_name}</center>
                          </td>
                          <td>
                            <center>{row.created_date}</center>
                          </td>
                          <td>
                            <center>{row.created_by}</center>
                          </td>
                          <td>
                            <center>{row.action}</center>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(ListSouvenir);
