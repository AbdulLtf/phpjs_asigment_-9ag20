import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { withRouter } from "react-router-dom";

class ViewSouvenir extends React.Component {
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.view}
          className={this.props.className}
          size="md"
        >
          <ModalHeader
            className="custom-modal-style"
            style={{ color: "#ffffff", backgroundColor: "#000066" }}
          >
            Souvenir ({this.props.souvenir.code})
          </ModalHeader>
          <ModalBody>
            <div className="container">
              <div className="row" style={{ width: "710px" }}>
                <div className="mb-2 col-lg-12" style={{ display: "block" }}>
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Souvnier Code</div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        disabled
                        placeholder={this.props.souvenir.code}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "5px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Souvenir Name : </div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        type="text"
                        disabled
                        placeholder={this.props.souvenir.name}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "5px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">Unit Name</div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        disabled
                        placeholder={this.props.souvenir.un_name}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "3px"
                        }}
                      />
                    </div>
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    <div className="col-3 d-inline-block">
                      Souvenir Description
                    </div>
                    <div className="col-1 d-inline-block">:</div>
                    <div className="col-3 d-inline-block">
                      <input
                        disabled
                        placeholder={this.props.souvenir.description}
                        style={{
                          width: "150px",
                          border: "solid black 0.5px",
                          borderRadius: "3px"
                        }}
                      />
                    </div>
                  </p>
                </div>
              </div>
            </div>
          </ModalBody>
          <ModalFooter style={{ backgroundColor: "#e9ecef" }}>
            <Button color="warning" onClick={this.props.closeModalHandler}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default withRouter(ViewSouvenir);
