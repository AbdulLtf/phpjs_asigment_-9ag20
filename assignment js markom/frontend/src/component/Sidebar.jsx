import React from "react";
// import { Link } from "react-router-dom";

class Sidebar extends React.Component {
  render() {
    return (
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="/" class="brand-link">
          Markom Project
        </a>
        <div class="sidebar">
          <nav class="mt-2">
            <ul
              class="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <li class="nav-item has-treeview menu-open">
                <a href="" class="nav-link active">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    List Menu
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>

                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/home" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p>home</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/product" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p>product</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/souvenir" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p>souvenir</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </aside>
    );
  }
}

export default Sidebar;
