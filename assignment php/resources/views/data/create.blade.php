@extends('layout/main')
@section('title','Halaman Data')
    
@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
    <h3>Form Tambah Data</h3>
    </div>
    <div>
    <form method="post" action="/data">
        @csrf
        <div class="form-group">
          <label for="firstname">Firstname</label>
        <input name="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" id="firstname" placeholder="Masukan Firstname" value="{{old('firstname')}}">
        </div>
        <div class="form-group">
          <label for="lastname">Lastname</label>
          <input name="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" id="lastname" placeholder="Masukan Lastname" value="{{old('lastname')}}">
        </div>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </form>
    </div>
</div>
@endsection

