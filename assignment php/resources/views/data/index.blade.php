@extends('layout/main')
@section('title','Halaman Data')
    
@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
    <h3>Data User</h3>
      <a href="data/create" class="btn btn-primary">Tambah Data</a>
      @if(session('Status'))
        <div class="alert alert-success">
            {{session('Status')}}
        </div>
      @endif
    </div>
    <div>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Firstname</th>
            <th scope="col">Lastname</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $datas)
              
          <tr>
          <th scope="row">{{$loop->iteration}}</th>
          <td>{{$datas->firstname}}</td>
            <td>{{$datas->lastname}}</td>
            <td>
            <a href="/data/{{$datas->id}}" class="badge badge-info">detail</a>
              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

