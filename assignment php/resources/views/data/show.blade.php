@extends('layout/main')
@section('title','Detail Data')
    
@section('container')
<div class="container">
<div>Detail Data</div>
<div class="card" style="width: 18rem;">
    <ul class="list-group list-group-flush">
    <li class="list-group-item">{{$data->id}}</li>
      <li class="list-group-item">{{$data->firstname}}</li>
      <li class="list-group-item">{{$data->lastname}}</li>
    </ul>
    <div>
      <a href="{{$data->id}}/edit" class="btn btn-info">Edit</a>
    <form action="{{$data->id}}" method="post" class="d-inline">
      @method('delete')
      @csrf
      <button type="submit" class="btn btn-danger">delete</button>
    </form>
    
    <a href="/data">Kembali</a>
  </div>
  </div>
</div>
@endsection