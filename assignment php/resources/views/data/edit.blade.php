@extends('layout/main')
@section('title','Halaman Data')
    
@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
    <h3>Form Edit Data</h3>
    </div>
    <div>
    <form method="post" action="/data/{{$data->id}}">
        @method('patch')
        @csrf
        <div class="form-group">
          <label for="firstname">Firstname</label>
        <input name="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" id="firstname" placeholder="Masukan Firstname" value="{{$data->firstname}}">
        </div>
        <div class="form-group">
          <label for="lastname">Lastname</label>
          <input name="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" id="lastname" placeholder="Masukan Lastname" value="{{$data->lastname}}">
        </div>
        <button type="submit" class="btn btn-primary">Edit Data</button>
      </form>
    </div>
</div>
@endsection

