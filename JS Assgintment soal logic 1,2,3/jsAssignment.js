var rl = require("readline-sync");
pilihan();
function pilihan() {
  let pilihan = parseInt(rl.question("Pilih soal: "));
  switch (pilihan) {
    case 1:
      soal1();
      break;
    case 2:
      soal2();
      break;
    case 3:
      soal3();
      break;
    case 4:
      soal4();
      break;
    case 5:
      soal5();
      break;
    case 6:
      soal6();
      break;
    default:
      console.log("Pilihan tidak ada");
  }
}

function ulang(soal) {
  let status = true;
  while (status) {
    let ulang = rl.question("ulang?([Y/N]): ");
    console.log(" ");
    if (ulang === "y" || ulang === "Y") {
      switch (soal) {
        case "soal1":
          soal1();
          break;
        case "soal2":
          soal2();
          break;
        case "soal3":
          soal3();
          break;
        case "soal4":
          soal4();
          break;
        case "soal5":
          soal5();
          break;
        case "soal6":
          soal6();
          break;
      }
    } else if (ulang === "n" || ulang === "N") {
      let menu = rl.question("ingin ke menu awal? ([Y/N]): ");
      console.log(" ");
      if (menu === "y" || menu === "Y") {
        pilihan();
      } else {
        break;
      }
    } else {
      break;
    }
    break;
  }
}

function soal1() {
  let batas = rl.question("Masukan batas angka: ");
  console.log(" ");
  let arr = [];
  if (batas) {
    for (i = 1; i <= batas; i++) {
      if (i % 3 === 0 && i % 4 === 0) {
        arr.push("OKYES");
      } else if (i % 4 === 0) {
        arr.push("YES");
      } else if (i % 3 === 0) {
        arr.push("OK");
      } else {
        arr.push(i);
      }
    }
    console.log(arr);
    console.log(" ");
  } else {
    console.log("anda tidak memasukan angka");
    console.log(" ");
    soal1();
  }

  ulang("soal1");
}

function soal2() {
  let baris = parseInt(rl.question("Masukan panjang baris: "));
  for (var i = 0; i < baris; i++) {
    space = baris - 1 - i;
    value = i + 1;
    stair = value.toString().repeat(value) + " ".repeat(space);
    console.log(stair);
  }
  ulang("soal2");
}

function soal3() {
  let baris = parseInt(rl.question("Masukan panjang baris: "));
  var output = "";
  for (var i = 1; i <= baris; i++) {
    for (var j = i; j >= 1; j--) {
      output += j + " ";
    }
    console.log(output);
    output = "";
  }
  ulang("soal3");
}

function soal4() {
  let baris = parseInt(rl.question("Masukan panjang baris: "));
  var output = "";
  for (var i = 1; i <= baris; i++) {
    for (var j = i; j >= 1; j--) {
      output += j + " ";
    }
    console.log(output);
    output = "";
  }
  ulang("soal4");
}
// 12345;

function soal5() {
  let angka = parseInt(rl.question("Masukan panjang baris: "));
  var a = 1;
  for (var i = 1; i <= angka; i++) {
    let num = "";
    for (var j = 1; j <= angka; j++) {
      num += a + " ";
      a++;
    }
    console.log(num);
  }
  ulang("soal5");
}

function soal6() {
  let kalimat = rl.question("Masukan kalimat: ");
  let change = kalimat.replace(/r/g, "l").replace(/R/g, "L");
  console.log(change);
  ulang("soal6");
}
